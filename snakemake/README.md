# Snakemake workflow for XenoCell

[![Python 3.6](https://img.shields.io/badge/python-3.6-blue.svg)](https://www.python.org/downloads/release/python-360/)
[![Snakemake](https://img.shields.io/badge/snakemake-≥4.0.0-brightgreen.svg)](https://snakemake.bitbucket.io)
[![Docker Pulls](https://img.shields.io/docker/pulls/romanhaa/xenocell.svg)](https://hub.docker.com/r/romanhaa/xenocell)
[![Twitter URL](https://img.shields.io/twitter/url/https/twitter.com/lost_efano.svg?label=Stefano&style=social)](https://twitter.com/lost_efano)
[![Twitter URL](https://img.shields.io/twitter/url/https/twitter.com/fakechek1.svg?label=Roman&style=social)](https://twitter.com/fakechek1)

Takes FASTQ files from paired-end droplet scRNA-seq experiments and generates filtered FASTQ files through XenoCell.

# Software Requirements

* `Python` v3.6 or above.
* `Snakemake` v4.0.0 or above.
* `Singularity` v3.1.1 or above.
* A Built XenoCell `docker` container. (For details please refer to the respective [README file](../README.md))

# How to run

### 1. Invoking the full snakemake workflow on a single machine.

```
# show commands and workflow
snakemake --snakefile XenoCell/snakemake/XenoCell_pipeline.snakefile -np \
--directory <main_directory_output_path> \
--configfile <path_to_your_config.yaml_file>
# run workflow
snakemake --snakefile XenoCell/snakemake/XenoCell_pipeline.snakefile \
--directory <main_directory_output_path> \
--configfile <path_to_your_config.yaml_file>
```

### 2. Invoking the snakemake workflow from a cluster.

The pipeline is launched through an helper bash script that will initiate and control all the steps and actions.

```bash
qsub XenoCell/snakemake/XenoCell_hlpr.sh
```

In this repository this file is provided with the `qsub` cluster configuration. If needed, the workflow can be easily converted to `slurm` or any other cluster configuration, following [snakemake](https://snakemake.readthedocs.io/en/stable/snakefiles/configuration.html) manual.

In the `XenoCell_hlpr.sh` script the user must specify `--directory` and `--config` snakemake arguments, indicating the path of the main output directory and the analysis [configuration file](### configuration file), respectively. A cluster configuration file (in our case `--cluster-config pbs.json`) must be provided, specifying for each rule the amount of resources to dedicate. Finally the `--cluster` parameter allows to submit the jobs to your scheduler. For further details see [Executing snakemake workflows](https://snakemake.readthedocs.io/en/stable/executable.html) documentation.

### Configuration file

Whether the workflow is run from a single machine or a cluster, a [`config.yaml`](./config.yaml) file must be provided from the user.
In this file the user provides samples information and defines the desired subsets thresholds.
Other parameters can be set such as barcode starting position and length as well as reference genomes paths.

# Acknowledgements

* Köster J. & Rahmann S. (2012) "Snakemake - A scalable bioinformatics workflow engine." *Bioinformatics*. [Article](https://doi.org/10.1093/bioinformatics/bts480) | [Docs](https://snakemake.readthedocs.io/en/stable/)
