import collections
import gzip
import pandas as pd
import sys

sys.dont_write_bytecode = True


def print_log(message):
  """Print message with current timestamp."""
  import time
  print('[' + time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime()) + '] ' + message)


def create_directory(dir):
  """Create directory if it doesn't exist already."""
  import os
  if not os.path.exists(dir):
    os.makedirs(dir)


def is_tool(tool):
  """Check whether "tool" is in PATH variable and can be executed."""
  from shutil import which
  return which(tool) is not None


def build_command_compression(threads):
  """Build compression command using either gzip or pigz.
  
  Which tool to use depends on how many threads should be used.
  """
  if (is_tool('pigz')) and (threads > 1):
    command_compression = 'pigz -p {threads}'.format(threads = threads)
  else:
    command_compression = 'gzip'
  return(command_compression)


def build_command_decompression(threads):
  """Build decompression command using either gunzip or unpigz.

  Which tool to use depends on how many threads should be used.
  """
  if (is_tool('unpigz')) and (threads > 1):
    command_decompression = 'unpigz -p {threads}'.format(threads = threads)
  else:
    command_decompression = 'gunzip'
  return(command_decompression)


def compress_file(file, compression_level, threads):
  """Compress a file."""
  import os
  command = '''
    {command_compression} -{compression_level} {file}
  '''.format(
    command_compression = build_command_compression(threads),
    compression_level   = compression_level,
    file                = file
  )
  os.system(command)


def decompress_file(file_in, file_out, threads):
  """Decompress a file."""
  import os
  command = '''
    {command_decompression} -c {file_in} > {file_out}
  '''.format(
    command_decompression = build_command_decompression(threads),
    file_in               = file_in,
    file_out              = file_out,
  )
  os.system(command)


def copy_file(file_in, file_out):
  """Copy a file to a desired location."""
  import os
  command = '''
    cp {file_in} {file_out}
  '''.format(
    file_in  = file_in,
    file_out = file_out
  )
  os.system(command)


def read_file(file, barcode_start_position, barcode_end_position):
  """Extract barcodes from a FASTQ file and return them as a data frame."""
  cellular_barcodes = []
  with open(file, 'r') as fastq:
    for lineno, line in enumerate(fastq):
      if lineno % 4 == 1:
        current_barcode = line.strip('\n')[barcode_start_position:barcode_end_position]
        cellular_barcodes.append(current_barcode)
  counts = collections.Counter(cellular_barcodes)
  df = pd.DataFrame.from_dict(counts, orient = 'index').reset_index()
  return df
