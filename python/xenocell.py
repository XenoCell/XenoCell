#!/opt/conda/bin/python

##----------------------------------------------------------------------------##
## Available commands:
## - Create Xenome index from a pair of reference genomes
## - Classify reads (graft/host)
## - Extract cellular barcodes based on given thresholds for percentage of
##   host-specific reads
##----------------------------------------------------------------------------##

#
import argparse
import common_functions
import sys
from generate_index import generate_index
from classify_reads import classify_reads
from extract_cellular_barcodes import extract_cellular_barcodes

#
sys.dont_write_bytecode = True


#
def main():

  # top level parser
  parser = argparse.ArgumentParser()
  subparsers = parser.add_subparsers(metavar  = '')

  parser_generate_index(subparsers)
  parser_classify_reads(subparsers)
  parser_extract_cellular_barcodes(subparsers)

  args = parser.parse_args()
  args.func(args)


#
def parser_generate_index(subparsers):
  ##--------------------------------------------------------------------------##
  ## Generate Xenome index of reference genomes.
  ##--------------------------------------------------------------------------##
  ## Arguments:
  ## --host
  ## --graft
  ## --output
  ## --threads
  ## --memory
  ##--------------------------------------------------------------------------##
  parser = subparsers.add_parser(
    name = 'generate_index',
    help = 'Generate index for Xenome.'
  )

  parser._action_groups.pop()

  parser_required = parser.add_argument_group('required arguments')
  parser_optional = parser.add_argument_group('optional arguments')

  parser_required.add_argument(
    '--host',
    type     = str,
    metavar  = '<STR>',
    required = True,
    help     = 'Path to FASTA file of host reference genome.'
  )
  parser_required.add_argument(
    '--graft',
    type     = str,
    metavar  = '<STR>',
    required = True,
    help     = 'Path to FASTA file of graft reference genome.'
  )
  parser_required.add_argument(
    '--output',
    type     = str,
    metavar  = '<STR>',
    required = True,
    help     = 'Path to output directory.'
  )
  parser_optional.add_argument(
    '--threads',
    type     = int,
    metavar  = '<INT>',
    required = False,
    default  = 1,
    help     = 'Number of threads; defauts to 1.'
  )
  parser_optional.add_argument(
    '--memory',
    type     = int,
    metavar  = '<INT>',
    required = False,
    default  = 16,
    help     = 'Memory in GB; defaults to 16.'
  )

  parser.set_defaults(func = generate_index)


#
def parser_classify_reads(subparsers):
  ##--------------------------------------------------------------------------##
  ## Classify reads and generate table of cellular barcodes.
  ##--------------------------------------------------------------------------##
  ## Arguments:
  ## --transcript
  ## --barcode
  ## --barcode_start
  ## --barcode_length
  ## --index
  ## --output
  ## --threads
  ## --memory
  ##--------------------------------------------------------------------------##
  parser = subparsers.add_parser(
    name = 'classify_reads',
    help = 'Classify reads with Xenome and aggregate reads per cell.'
)

  parser._action_groups.pop()

  parser_required = parser.add_argument_group('required arguments')
  parser_optional = parser.add_argument_group('optional arguments')

  parser_required.add_argument(
    '--transcript',
    type     = str,
    metavar  = '<STR>',
    required = True,
    help     = 'Path to FASTQ file containing transcript.'
  )
  parser_required.add_argument(
    '--barcode',
    type     = str,
    metavar  = '<STR>',
    required = True,
    help     = 'Path to FASTQ file containing cellular barcode.'
  )
  parser_required.add_argument(
    '--barcode_start',
    type     = int,
    metavar  = '<INT>',
    required = True,
    help     = 'Position in barcode-containing read at which cellular ' +
               'barcode starts; 1-based counting.'
  )
  parser_required.add_argument(
    '--barcode_length',
    type     = int,
    metavar  = '<INT>',
    required = True,
    help     = 'Length of cellular barcode.'
  )
  parser_required.add_argument(
    '--index',
    type     = str,
    metavar  = '<STR>',
    required = True,
    help     = 'Path to Xenome index.'
  )
  parser_required.add_argument(
    '--output',
    type     = str,
    metavar  = '<STR>',
    required = True,
    help     = 'Output directory.'
  )
  parser_optional.add_argument(
    '--keep_xenome_output',
    type     = bool,
    metavar  = '<BOOL>',
    required = False,
    default  = False,
    help     = 'Keep output of Xenome, e.g. _graft.fq and _host.fq; defaults ' +
               'to False.'
  )
  parser_optional.add_argument(
    '--threads',
    type     = int,
    metavar  = '<INT>',
    required = False,
    default  = 1,
    help     = 'Number of threads; defaults to 1.'
  )
  parser_optional.add_argument(
    '--memory',
    type     = int,
    metavar  = '<INT>',
    required = False,
    default  = 16,
    help     = 'Memory in GB; defaults to 16.'
  )
  parser_optional.add_argument(
    '--compression_level',
    type     = int,
    metavar  = '<INT>',
    required = False,
    default  = 1,
    help     = 'Compression level used by gzip/pigz to compress FASTQ files. ' +
               'Choose any integer between 1 and 9. Low values favor speed ' +
               'over compression rate. Defaults to 1.'
  )
  parser.set_defaults(func = classify_reads)


#
def parser_extract_cellular_barcodes(subparsers):
  ##--------------------------------------------------------------------------##
  ## Extract cellular barcodes.
  ##--------------------------------------------------------------------------##
  ## Arguments:
  ## --input
  ## --barcode_start
  ## --barcode_length
  ## --subset_name
  ## --lower_threshold
  ## --upper_threshold
  ## --threads
  ##--------------------------------------------------------------------------##
  parser = subparsers.add_parser(
    name = 'extract_cellular_barcodes',
    help = 'Extract cellular barcodes with defined fraction of host-originating reads.'
)

  parser._action_groups.pop()

  parser_required = parser.add_argument_group('required arguments')
  parser_optional = parser.add_argument_group('optional arguments')

  parser_required.add_argument(
    '--input',
    type     = str,
    metavar  = '<STR>',
    required = True,
    default  = '',
    help     = 'Path to output directory of "classify_reads" step.'
  )
  parser_required.add_argument(
    '--barcode_start',
    type     = int,
    metavar  = '<INT>',
    required = True,
    help     = 'Position in barcode-containing read at which cellular barcode starts; 1-based counting.'
  )
  parser_required.add_argument(
    '--barcode_length',
    type     = int,
    metavar  = '<INT>',
    required = True,
    help     = 'Length of cellular barcode.'
  )
  parser_required.add_argument(
    '--subset_name',
    type     = str,
    metavar  = '<STR>',
    required = True,
    help     = 'Name of the subset of cellular barcodes. The output will be renamed using this suffix.'
  )
  parser_required.add_argument(
    '--lower_threshold',
    type     = float,
    metavar  = '<FLT>',
    required = True,
    help     = 'Extract cellular barcodes with a fraction of host-specific reads larger than this value, e.g. 0.0.'
  )
  parser_required.add_argument(
    '--upper_threshold',
    type     = float,
    metavar  = '<FLT>',
    required = True,
    help     = 'Extract cellular barcodes with a fraction of host-specific reads smaller than this value, e.g. 0.1.'
  )
  parser_optional.add_argument(
    '--threads',
    type     = int,
    metavar  = '<INT>',
    required = False,
    default  = 1,
    help     = 'Number of threads; defaults to 1.'
  )
  parser_optional.add_argument(
    '--compression_level',
    type     = int,
    metavar  = '<INT>',
    required = False,
    default  = 1,
    help     = 'Compression level used by gzip/pigz to compress FASTQ files. ' +
               'Choose any integer between 1 and 9. Low values favor speed ' +
               'over compression rate. Defaults to 1.'
  )
  parser.set_defaults(func = extract_cellular_barcodes)


#
if __name__ == '__main__':
  main()
