##----------------------------------------------------------------------------##
## Extract cellular barcodes.
##----------------------------------------------------------------------------##
## User input:
##   - output folder of step A
##   - subset name
##   - starting position of barcode
##   - length of barcode
##   - lower threshold value, e.g. 0
##   - upper threshold value, e.g. 0.1
##   - threads
##   - compression level
##----------------------------------------------------------------------------##
## Output:
##   - plot highlighting the cellular barcodes that will be extracted
##   - pair of FASTQ files containing reads belonging to the cellular barcodes
##     within the specified range of tresholds
##----------------------------------------------------------------------------##

#
import gzip
import matplotlib
import matplotlib.pyplot as plt
import os
import pandas as pd
import sys
import time

from common_functions import compress_file
from common_functions import create_directory
from common_functions import print_log

#
matplotlib.use('Agg')

#
sys.dont_write_bytecode = True

#
def extract_cellular_barcodes(args):

  print_header(args)

  # set current working directory to input folder
  os.chdir(args.input)

  # create folder for output FASTQ files
  output_directory = args.subset_name + '/'
  create_directory(output_directory)

  #
  print_log('Load output data from read classification step...')
  df_barcodes = load_barcode_table('host_vs_graft_total.csv')
  plot_subset_highlighted(df_barcodes, args.lower_threshold, args.upper_threshold, args.subset_name)

  #
  print_log('Generate list of cellular barcodes that pass the specified thresholds...')
  df_barcodes_filtered = df_barcodes.query('perc >= @args.lower_threshold').query('perc <= @args.upper_threshold')
  df_barcodes_filtered.to_csv(args.subset_name + '/cellular_barcodes.txt',
    sep = '\t', header = False, index = False, columns = ['barcode'],
    encoding = 'utf-8')
  list_barcodes_filtered = df_barcodes_filtered.iloc[:,0].tolist()
  dict_barcodes_filtered = dict((bc,1) for bc in list_barcodes_filtered)

  #
  print_log('Generate list of reads that belong to the cellular barcodes...')
  number_of_reads = generate_list_of_reads('fq_barcode.fq.gz',
    args.barcode_start, args.barcode_length, args.subset_name,
    dict_barcodes_filtered)

  #
  print_log('Extract reads from original FASTQ files...')
  extract_reads('fq_barcode.fq.gz', args.subset_name, 'fq_barcode.fq')
  extract_reads('fq_transcript.fq.gz', args.subset_name, 'fq_transcript.fq')

  #
  print_log('Compress FASTQ files...')
  compress_file(args.subset_name+'/fq_barcode.fq', args.compression_level, args.threads)
  compress_file(args.subset_name+'/fq_transcript.fq', args.compression_level, args.threads)

  #
  print_log('Delete temporary files...')
  delete_temp_files(args.subset_name)

  #
  print_log('Extraction of cellular barcodes finished!')
  print(' - Number of unique cellular barcodes extracted: ' + "{:,}".format(len(df_barcodes_filtered)))
  print(' - Number of reads extracted: ' + "{:,}".format(number_of_reads))



#
def print_header(args):
  header = '''##----------------------------------------------------------------------------##
## XenoCell: Extract cellular barcodes.
##----------------------------------------------------------------------------##
## Input folder:               {input}
## Barcode starting position:  {barcode_start}
## Length of cellular barcode: {barcode_length}
## Subset name:                {subset_name}
## Lower threshold:            {lower_threshold}
## Upper threshold:            {upper_threshold}
## Number of threads:          {threads}
## Compression level:          {compression_level}
##----------------------------------------------------------------------------##'''.format(
    input             = args.input,
    barcode_start     = args.barcode_start,
    barcode_length    = args.barcode_length,
    subset_name       = args.subset_name,
    lower_threshold   = args.lower_threshold,
    upper_threshold   = args.upper_threshold,
    threads           = args.threads,
    compression_level = args.compression_level
  )
  print(header)


#
def load_barcode_table(file):
  if not os.path.isfile(file):
    raise RuntimeError('Expected file ({file}) cannot be found.'.format(file=file))
  csv = pd.read_csv(file)
  return(csv)


# create plot of cellular barcodes and percentage of host-specific reads and
# highlight those cells that pass specified thresholds
def plot_subset_highlighted(df, lower_threshold, upper_threshold, subset_name):
  fig, ax = plt.subplots()
  ax.axhline(y = lower_threshold, color = 'black', linestyle = ':')
  ax.axhline(y = upper_threshold, color = 'black', linestyle = ':')
  barcodes_out = [i for i, val in enumerate(df['perc']) if val <= lower_threshold or val >= upper_threshold]
  barcodes_in = [i for i, val in enumerate(df['perc']) if val >= lower_threshold and val <= upper_threshold]
  ax.scatter(df['total'][barcodes_out], df['perc'][barcodes_out], color = 'black', marker = 'o')
  ax.scatter(df['total'][barcodes_in], df['perc'][barcodes_in], color = 'red', marker = 'o')
  plt.xlabel('Number of reads')
  plt.ylabel('Percentage of host-specific reads')
  plt.savefig('host_vs_graft_total_subset_' + subset_name + '.png')


# extract read names from fq_barcode.fastq that are on the list of cellular
# barcodes that should be extracted
def generate_list_of_reads(fastq_in, barcode_start, barcode_length,
  subset_name, dict_reads_to_extract):

  if not os.path.isfile(fastq_in):
    raise RuntimeError('Input FASTQ file ({file}) cannot be found.'.format(file=fastq_in))

  # define where to check for cellular barcode
  barcode_start_position = barcode_start - 1
  barcode_end_position = barcode_start_position + barcode_length

  # stop and raise error if barcode start position is too small
  if barcode_start_position < 0:
    raise ValueError('Barcode start position must be 1 or larger.')

  # open barcode-containing FASTQ file and check length of length of barcode-containing read
  # raise error if barcode end position is greater than length of barcode-containing read
  with gzip.open(fastq_in, 'r') as fastq:
    fastq.readline()
    read = fastq.readline()
    read_length = len(read.decode('utf-8').rstrip("\n\r"))
    if barcode_end_position > read_length:
      raise ValueError('Barcode end position ({end_position}) seems to be larger than read length ({read_length}).'.format(end_position = barcode_end_position, read_length = read_length))

  # define output file name
  output_read_names = subset_name + '/reads.txt'

  # delete output file if it already exists
  if os.path.exists(output_read_names) and os.path.isfile(output_read_names):
    os.remove(output_read_names) 

  # go through FASTQ file
  read_names_to_extract = []
  with gzip.open(fastq_in, 'r') as fastq:
    read_count = 0
    for lineno, line in enumerate(fastq):
      # line containing the read name
      if lineno % 4 == 0:
        # split line, remove @ and add newline char
        current_read_name = line.decode('utf-8').split(' ')[0].replace('@','',1) + '\n'
      # line containing sequence
      elif lineno % 4 == 1:
        # check if current cellular barcode is in dictionary of cellular barcodes to extract
        if line.decode('utf-8').rstrip('\n')[barcode_start_position:barcode_end_position] in dict_reads_to_extract:
          read_names_to_extract.append(current_read_name)
          read_count += 1
          # append read names to file when reaching 1 million reads
          if read_count == 10000000:
            with open(output_read_names, 'a') as new_fastq_c_graft_reads:
              new_fastq_c_graft_reads.write(''.join(read_names_to_extract))
            read_count = 0
            read_names_to_extract = []

  # append leftover read names to file
  if len(read_names_to_extract) > 0:
    with open(output_read_names, 'a') as new_fastq_c_graft_reads:
      new_fastq_c_graft_reads.write(''.join(read_names_to_extract))

  # check whether output file was produced
  if not os.path.isfile(output_read_names):
    raise RuntimeError('Expected output file ({file}) not found.'.format(file=output_read_names))

  # count number reads
  with open(output_read_names) as f:
    for i, l in enumerate(f):
      pass
  return i + 1


# create corresponding fq_barcode and fq_transcript files
def extract_reads(fastq_in, subset_name, fastq_out):
  if not os.path.isfile(fastq_in):
    raise RuntimeError('Input FASTQ file ({file}) cannot be found.'.format(file=fastq_in))
  command = '''
    /opt/conda/bin/seqtk subseq {fastq_in} {subset_name}/reads.txt > {subset_name}/tmp_unsorted.fq
    /opt/conda/bin/fastq-sort {subset_name}/tmp_unsorted.fq > {subset_name}/{fastq_out}
  '''.format(
    fastq_in    = fastq_in,
    subset_name = subset_name,
    fastq_out   = fastq_out
  )
  os.system(command)
  if not os.path.isfile(subset_name+'/'+fastq_out):
    raise RuntimeError('Expected output file ({file}) not found.'.format(file=fastq_out))


#
def delete_temp_files(subset_name):
  os.remove(subset_name + '/tmp_unsorted.fq')
  os.remove(subset_name + '/reads.txt')
