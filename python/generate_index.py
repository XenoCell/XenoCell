##----------------------------------------------------------------------------##
## Generate Xenome index of reference genomes.
##----------------------------------------------------------------------------##
## User input:
##   - host reference genome
##   - graft reference genome
##   - output folder
##   - number of threads
##   - memory in GB
##----------------------------------------------------------------------------##
## Output:
##   - index for Xenome
##----------------------------------------------------------------------------##

#
import os
import sys
import time

from common_functions import create_directory
from common_functions import print_log

#
sys.dont_write_bytecode = True

#
def generate_index(args):

  print_header(args)
  print_log('Generate index...')
  create_directory(args.output)
  run_xenome(args)
  print_log('Generation of Xenome index finished!')


#
def print_header(args):
  header = '''##----------------------------------------------------------------------------##
## XenoCell: Generate Xenome index of reference genomes.
##----------------------------------------------------------------------------##
## FASTA of host reference genome:  {host}
## FASTA of graft reference genome: {graft}
## Output directory:                {output}
## Number of threads:               {threads}
## Memory in GB:                    {memory}
##----------------------------------------------------------------------------##'''.format(
    host    = args.host,
    graft   = args.graft,
    output  = args.output,
    threads = args.threads,
    memory  = args.memory
  )
  print(header)


#
def run_xenome(args):
  command = '''
    /XenoCell/xenome-1.0.1-r/xenome index \
    -H {host} \
    -G {graft} \
    -T {threads} \
    -M {memory} \
    -P {output}/idx \
    -v
  '''.format(
    host    = args.host,
    graft   = args.graft,
    threads = args.threads,
    memory  = args.memory,
    output  = args.output
  )
  os.system(command)

