# `hgmm_5k_v3` aligned to human reference genome (hg19)

Here we perform down-stream analysis of the human (graft) cells extracted by XenoCell and compare their transcriptional profiles to the unfiltered sample.

## Analysis

Load libraries.

```r
library("Seurat")
library("tidyverse")

set.seed(1234567)
```

Load counts for unfiltered sample.

```r
feature_matrix_unfiltered <- Read10X_h5("../../data/hg19/unfiltered/filtered_feature_bc_matrix.h5")

genes <- rownames(feature_matrix_unfiltered)

colnames(feature_matrix_unfiltered) <- gsub(colnames(feature_matrix_unfiltered), pattern = "-1", replacement = "")
colnames(feature_matrix_unfiltered) <- paste0(colnames(feature_matrix_unfiltered), "-unfiltered")
feature_matrix_unfiltered <- feature_matrix_unfiltered %>%
  as.matrix() %>%
  as.data.frame() %>%
  mutate(gene = genes)
```

Load counts for graft-specific cells.

```r
feature_matrix_graft <- Read10X_h5("../../data/hg19/graft/filtered_feature_bc_matrix.h5")

genes <- rownames(feature_matrix_graft)

colnames(feature_matrix_graft) <- gsub(colnames(feature_matrix_graft), pattern = "-1", replacement = "")
colnames(feature_matrix_graft) <- paste0(colnames(feature_matrix_graft), "-graft")

feature_matrix_graft <- feature_matrix_graft %>%
  as.matrix() %>%
  as.data.frame() %>%
  mutate(gene = genes)
```

Merge counts.

```r
feature_matrix <- full_join(feature_matrix_unfiltered, feature_matrix_graft, by = "gene")
feature_matrix[ is.na(feature_matrix) ] <- 0
genes <- feature_matrix$gene

feature_matrix %<>%
  select(-c("gene")) %>%
  as.matrix()
rownames(feature_matrix) <- genes
```

Create Seurat object.

```r
seurat <- CreateSeuratObject(
  project = "hgmm_5k_v3_hg19",
  counts = feature_matrix,
  min.cells = 10
)
```

Add sample info to meta data.

```r
seurat@meta.data$sample = factor(
  vapply(strsplit(rownames(seurat@meta.data), "-"), `[`, 2, FUN.VALUE = character(1)),
  levels = c("unfiltered","graft")
)

summary(seurat@meta.data$sample)
# unfiltered      graft
#       5232       2532
```

Normalize and log-transform transcript counts, find variable genes, scale data (regressing out the number of transcripts in each cell) and perform PCA.

```r
seurat <- NormalizeData(seurat)
seurat <- FindVariableFeatures(seurat)
seurat <- ScaleData(seurat, vars.to.regress = "nCount_RNA")
seurat <- RunPCA(seurat, npcs = 30, features = seurat@assays$RNA@var.features)
```

Generate UMAP projection with the first 30 principal components and plot.

```r
seurat <- RunUMAP(seurat, reduction.name = "UMAP", dims = 1:30, seed.use = 100)

p <- cbind(seurat@meta.data, seurat@reductions$UMAP@cell.embeddings) %>%
  ggplot(aes(UMAP_1, UMAP_2, color = sample)) +
  geom_point(alpha = 0.5) +
  theme_bw() +
  scale_color_manual(values = c("unfiltered" = "#7f8c8d", "graft" = "#3498db", "host" = "#e74c3c")) +
  labs(color = "Sample")

ggsave("UMAP_by_sample.pdf", p, height = 6, width = 7)
ggsave("UMAP_by_sample.png", p, height = 6, width = 7)

p <- cbind(seurat@meta.data, seurat@reductions$UMAP@cell.embeddings) %>%
  ggplot(aes(UMAP_1, UMAP_2, color = sample)) +
  geom_point(show.legend = FALSE) +
  theme_bw() +
  labs(color = "Sample") +
  scale_color_manual(values = c("unfiltered" = "#7f8c8d", "graft" = "#3498db", "host" = "#e74c3c")) +
  facet_wrap(~sample)

ggsave("UMAP_by_sample_split.pdf", p, height = 6, width = 16)
ggsave("UMAP_by_sample_split.png", p, height = 6, width = 16)
```

![UMAP_by_sample](UMAP_by_sample.png)
![UMAP_by_sample_split](UMAP_by_sample_split.png)

Save Seurat object as `RDS` file.

```r
saveRDS(seurat, "seurat.rds")
```

## Session info

```r
R version 3.5.2 (2018-12-20)
Platform: x86_64-pc-linux-gnu (64-bit)
Running under: Debian GNU/Linux buster/sid

Matrix products: default
BLAS: /usr/lib/x86_64-linux-gnu/blas/libblas.so.3.8.0
LAPACK: /usr/lib/x86_64-linux-gnu/lapack/liblapack.so.3.8.0

locale:
 [1] LC_CTYPE=en_US.UTF-8       LC_NUMERIC=C              
 [3] LC_TIME=en_US.UTF-8        LC_COLLATE=en_US.UTF-8    
 [5] LC_MONETARY=en_US.UTF-8    LC_MESSAGES=en_US.UTF-8   
 [7] LC_PAPER=en_US.UTF-8       LC_NAME=C                 
 [9] LC_ADDRESS=C               LC_TELEPHONE=C            
[11] LC_MEASUREMENT=en_US.UTF-8 LC_IDENTIFICATION=C       

attached base packages:
[1] stats     graphics  grDevices utils     datasets  methods   base     

other attached packages:
 [1] forcats_0.4.0   stringr_1.4.0   dplyr_0.8.0.1   purrr_0.3.2    
 [5] readr_1.3.1     tidyr_0.8.3     tibble_2.1.1    ggplot2_3.1.1  
 [9] tidyverse_1.2.1 Seurat_3.0.0   

loaded via a namespace (and not attached):
 [1] nlme_3.1-139        tsne_0.1-3          bitops_1.0-6       
 [4] bit64_0.9-7         lubridate_1.7.4     RColorBrewer_1.1-2 
 [7] httr_1.4.0          sctransform_0.2.0   tools_3.5.2        
[10] backports_1.1.4     R6_2.4.0            irlba_2.3.3        
[13] KernSmooth_2.23-15  lazyeval_0.2.2      colorspace_1.4-1   
[16] withr_2.1.2         npsurv_0.4-0        tidyselect_0.2.5   
[19] gridExtra_2.3       bit_1.1-14          compiler_3.5.2     
[22] cli_1.1.0           rvest_0.3.3         hdf5r_1.2.0        
[25] xml2_1.2.0          plotly_4.9.0        labeling_0.3       
[28] caTools_1.17.1.2    scales_1.0.0        lmtest_0.9-36      
[31] ggridges_0.5.1      pbapply_1.4-0       digest_0.6.18      
[34] R.utils_2.8.0       pkgconfig_2.0.2     htmltools_0.3.6    
[37] bibtex_0.4.2        readxl_1.3.1        htmlwidgets_1.3    
[40] rlang_0.3.4         rstudioapi_0.10     generics_0.0.2     
[43] zoo_1.8-5           jsonlite_1.6        ica_1.0-2          
[46] gtools_3.8.1        R.oo_1.22.0         magrittr_1.5       
[49] Matrix_1.2-17       Rcpp_1.0.1          munsell_0.5.0      
[52] ape_5.3             reticulate_1.12     R.methodsS3_1.7.1  
[55] stringi_1.4.3       gbRd_0.4-11         MASS_7.3-51.3      
[58] gplots_3.0.1.1      Rtsne_0.15          plyr_1.8.4         
[61] grid_3.5.2          parallel_3.5.2      gdata_2.18.0       
[64] listenv_0.7.0       ggrepel_0.8.0       crayon_1.3.4       
[67] lattice_0.20-38     haven_2.1.0         cowplot_0.9.4      
[70] splines_3.5.2       hms_0.4.2           SDMTools_1.1-221.1 
[73] pillar_1.3.1        igraph_1.2.4.1      future.apply_1.2.0 
[76] reshape2_1.4.3      codetools_0.2-16    glue_1.3.1         
[79] lsei_1.2-0          metap_1.1           modelr_0.1.4       
[82] data.table_1.12.2   png_0.1-7           Rdpack_0.11-0      
[85] cellranger_1.1.0    gtable_0.3.0        RANN_2.6.1         
[88] future_1.12.0       assertthat_0.2.1    rsvd_1.0.0         
[91] broom_0.5.2         survival_2.44-1.1   viridisLite_0.3.0  
[94] cluster_2.0.8       globals_0.12.4      fitdistrplus_1.0-14
[97] ROCR_1.0-7
```












