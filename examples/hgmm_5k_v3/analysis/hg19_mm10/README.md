# Comparison of XenoCell results with droplet classification by Cell Ranger

Here we compare the results of droplet classification from XenoCell and Cell Ranger.

## Analysis

Load libraries.

```r
library("Seurat")
library("tidyverse")

set.seed(1234567)
```

Read Cell Ranger results.

```r
gem_classification <- read_csv("../../data/hg19_mm10/unfiltered/gem_classification.csv", col_names = TRUE)

```

Load graft and host cells.

```r
feature_matrix_graft <- Read10X_h5("../../data/hg19/graft/filtered_feature_bc_matrix.h5")
cells_graft <- colnames(feature_matrix_graft)

feature_matrix_host <- Read10X_h5("../../data/mm10/host/filtered_feature_bc_matrix.h5")
cells_host <- colnames(feature_matrix_host)
```

Classification of graft cells.

```r
length(cells_graft)
# 2532
length(which(gem_classification$barcode %in% cells_graft))
# 2448
summary(factor(gem_classification$call[which(gem_classification$barcode %in% cells_graft)]))
# hg19
# 2448
```

2,448 out of the 2,532 cells that XenoCell identified as originating from the host were also retrieved by Cell Ranger.
More importantly, all of those cells were labeled as human (`hg19`).

Classification of host cells.

```r
length(cells_host)
# 2626
length(which(gem_classification$barcode %in% cells_host))
# 2626
summary(factor(gem_classification$call[which(gem_classification$barcode %in% cells_host)]))
# mm10
# 2626
```

## Results

All cells that XenoCell identified as originating from the host were also retrieved by Cell Ranger and labeled concordantly as mouse (`mm10`).

## Session info

```r
R version 3.5.2 (2018-12-20)
Platform: x86_64-pc-linux-gnu (64-bit)
Running under: Debian GNU/Linux buster/sid

Matrix products: default
BLAS: /usr/lib/x86_64-linux-gnu/blas/libblas.so.3.8.0
LAPACK: /usr/lib/x86_64-linux-gnu/lapack/liblapack.so.3.8.0

locale:
 [1] LC_CTYPE=en_US.UTF-8       LC_NUMERIC=C
 [3] LC_TIME=en_US.UTF-8        LC_COLLATE=en_US.UTF-8
 [5] LC_MONETARY=en_US.UTF-8    LC_MESSAGES=en_US.UTF-8
 [7] LC_PAPER=en_US.UTF-8       LC_NAME=C
 [9] LC_ADDRESS=C               LC_TELEPHONE=C
[11] LC_MEASUREMENT=en_US.UTF-8 LC_IDENTIFICATION=C

attached base packages:
[1] stats     graphics  grDevices utils     datasets  methods   base

other attached packages:
 [1] forcats_0.4.0   stringr_1.4.0   dplyr_0.8.0.1   purrr_0.3.2
 [5] readr_1.3.1     tidyr_0.8.3     tibble_2.1.1    ggplot2_3.1.1
 [9] tidyverse_1.2.1 Seurat_3.0.0

loaded via a namespace (and not attached):
 [1] nlme_3.1-139        tsne_0.1-3          bitops_1.0-6
 [4] bit64_0.9-7         lubridate_1.7.4     RColorBrewer_1.1-2
 [7] httr_1.4.0          sctransform_0.2.0   tools_3.5.2
[10] backports_1.1.4     R6_2.4.0            irlba_2.3.3
[13] KernSmooth_2.23-15  lazyeval_0.2.2      colorspace_1.4-1
[16] withr_2.1.2         npsurv_0.4-0        tidyselect_0.2.5
[19] gridExtra_2.3       bit_1.1-14          compiler_3.5.2
[22] cli_1.1.0           rvest_0.3.3         hdf5r_1.2.0
[25] xml2_1.2.0          plotly_4.9.0        caTools_1.17.1.2
[28] scales_1.0.0        lmtest_0.9-36       ggridges_0.5.1
[31] pbapply_1.4-0       digest_0.6.18       R.utils_2.8.0
[34] pkgconfig_2.0.2     htmltools_0.3.6     bibtex_0.4.2
[37] readxl_1.3.1        htmlwidgets_1.3     rlang_0.3.4
[40] rstudioapi_0.10     generics_0.0.2      zoo_1.8-5
[43] jsonlite_1.6        ica_1.0-2           gtools_3.8.1
[46] R.oo_1.22.0         magrittr_1.5        Matrix_1.2-17
[49] Rcpp_1.0.1          munsell_0.5.0       ape_5.3
[52] reticulate_1.12     R.methodsS3_1.7.1   stringi_1.4.3
[55] gbRd_0.4-11         MASS_7.3-51.3       gplots_3.0.1.1
[58] Rtsne_0.15          plyr_1.8.4          grid_3.5.2
[61] parallel_3.5.2      gdata_2.18.0        listenv_0.7.0
[64] ggrepel_0.8.0       crayon_1.3.4        lattice_0.20-38
[67] haven_2.1.0         cowplot_0.9.4       splines_3.5.2
[70] hms_0.4.2           SDMTools_1.1-221.1  pillar_1.3.1
[73] igraph_1.2.4.1      future.apply_1.2.0  reshape2_1.4.3
[76] codetools_0.2-16    glue_1.3.1          lsei_1.2-0
[79] metap_1.1           modelr_0.1.4        data.table_1.12.2
[82] png_0.1-7           Rdpack_0.11-0       cellranger_1.1.0
[85] gtable_0.3.0        RANN_2.6.1          future_1.12.0
[88] assertthat_0.2.1    rsvd_1.0.0          broom_0.5.2
[91] survival_2.44-1.1   viridisLite_0.3.0   cluster_2.0.8
[94] globals_0.12.4      fitdistrplus_1.0-14 ROCR_1.0-7
```

