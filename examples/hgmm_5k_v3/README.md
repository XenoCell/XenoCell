# `hgmm_5k_v3` example data set

As an example, we applied XenoCell to a public data set downloaded from the [10x Genomics website](https://support.10xgenomics.com/single-cell-gene-expression/datasets/3.0.0/hgmm_5k_v3).
This data set is licensed under the [Creative Commons Attribution](https://creativecommons.org/licenses/by/4.0/) license.
It contains a mix of around 5,000 human and mouse cells mixed in a 1:1 ratio.

## Download FASTQ

```sh
wget http://s3-us-west-2.amazonaws.com/10x.files/samples/cell-exp/3.0.0/hgmm_5k_v3/hgmm_5k_v3_fastqs.tar
tar -xvf hgmm_5k_v3_fastqs.tar
```

Merge FASTQ files.

```sh
cat hgmm_5k_v3_S1_L001_R1_001.fastq.gz hgmm_5k_v3_S1_L002_R1_001.fastq.gz \
> hgmm_5k_v3_S1_R1_001.fastq.gz

cat hgmm_5k_v3_S1_L001_R2_001.fastq.gz hgmm_5k_v3_S1_L002_R2_001.fastq.gz \
> hgmm_5k_v3_S1_R2_001.fastq.gz
```

## XenoCell

### Generate Xenome index

```sh
singularity exec \
--bind /hpcnfs \
/hpcnfs/scratch/PGP/xenocell/container/xenocell_1.0.simg \
xenocell.py generate_index \
--host /hpcnfs/data/PGP/reference_genomes/UCSC/mm10/WholeGenomeFasta/mm10.fa \
--graft /hpcnfs/data/PGP/reference_genomes/UCSC/hg19/WholeGenomeFasta/hg19.fa \
--output /hpcnfs/data/PGP/reference_genomes/xenome/graft_hg19_host_mm10 \
--threads 16 \
--memory 60
##----------------------------------------------------------------------------##
## XenoCell: Generate Xenome index of reference genomes.
##----------------------------------------------------------------------------##
## FASTA of host reference genome:  /hpcnfs/data/PGP/reference_genomes/UCSC/mm10/WholeGenomeFasta/mm10.fa
## FASTA of graft reference genome: /hpcnfs/data/PGP/reference_genomes/UCSC/hg19/WholeGenomeFasta/hg19.fa
## Output directory:                /hpcnfs/data/PGP/reference_genomes/xenome/graft_hg19_host_mm10_test
## Number of threads:               16
## Memory in GB:                    60
##----------------------------------------------------------------------------##
# [2020-12-01 08:24:00] Generate index...
# [2020-12-01 10:17:51] Generation of Xenome index finished!
```

### Step A: Classify reads with XenoCell

We use the `classify_reads` function to classify reads and generate a table of cellular barcodes containing the percentage of graft- and host-specific reads for each of them.
In this case, the read that contains the transcript is `R2`, while `R1` contains the cellular barcode.

```sh
singularity exec \
--bind /hpcnfs \
/hpcnfs/scratch/PGP/xenocell/container/xenocell_1.0.simg \
xenocell.py classify_reads \
--transcript /hpcnfs/scratch/PGP/xenocell/FASTQ_files/hgmm_5k_v3/hgmm_5k_v3_S1_R2_001.fastq.gz \
--barcode /hpcnfs/scratch/PGP/xenocell/FASTQ_files/hgmm_5k_v3/hgmm_5k_v3_S1_R1_001.fastq.gz \
--barcode_start 1 \
--barcode_length 16 \
--index /hpcnfs/data/PGP/reference_genomes/xenome/1.0.1/graft_hg19_host_mm10 \
--output /hpcnfs/scratch/PGP/xenocell/test_runs/2020-12-01/hgmm_5k_v3 \
--threads 16 \
--memory 60 \
--compression_level 1
##----------------------------------------------------------------------------##
## XenoCell: Classify reads and generate table of cellular barcodes.
##----------------------------------------------------------------------------##
## FASTQ file containing transcript:       /hpcnfs/scratch/PGP/xenocell/FASTQ_files/hgmm_5k_v3/hgmm_5k_v3_S1_L001_R2_001.fastq.gz
## FASTQ file containing cellular barcode: /hpcnfs/scratch/PGP/xenocell/FASTQ_files/hgmm_5k_v3/hgmm_5k_v3_S1_L001_R1_001.fastq.gz
## Barcode starting position:              1
## Length of cellular barcode:             16
## Path to Xenome index:                   /hpcnfs/data/PGP/reference_genomes/xenome/graft_hg19_host_mm10
## Output directory:                       /hpcnfs/scratch/PGP/xenocell/test_runs/2020-12-01/hgmm_5k_v3
## Keep Xenome output files:               False
## Number of threads:                      16
## Memory in GB:                           60
## Compression level:                      1
##----------------------------------------------------------------------------##
# [2020-12-01 18:23:04] De-compress FASTQ files before classifying...
# [2020-12-01 18:45:38] Start classification...
# [2020-12-01 19:28:34] Fix FASTQ format...
# [2020-12-01 19:46:53] Create list of read names to extract...
# [2020-12-01 19:58:36] Extract reads...
# [2020-12-01 20:21:51] Generate table of cellular barcodes and associated read counts...
# [2020-12-01 20:30:50] Number of unique cellular barcodes: 5,254,674
# [2020-12-01 20:30:50] Compress FASTQ files...
# [2020-12-01 20:50:55] Delete temporary files...
# [2020-12-01 20:50:57] Classification of reads finished!
```

This step creates a plot showing the fraction of graft/host-specific reads for each cellular barcode.

<p align="center"><img src="host_vs_graft_total.png" /></p>

Based on this plot, we can decide the thresholds when extracting cellular barcodes.

### Step B: Extract graft-specific cellular barcodes

To extract **graft**-specific cellular barcodes, we use the `extract_cellular_barcodes` function to extract reads from all cellular barcodes that contain between 0-10% of host-specific reads.

__Note__:
We could omit the `--lower_threshold` parameter since the default is `0.0`.

```sh
singularity exec \
--bind /hpcnfs \
/hpcnfs/scratch/PGP/xenocell/container/xenocell_1.0.simg \
xenocell.py extract_cellular_barcodes \
--input /hpcnfs/scratch/PGP/xenocell/test_runs/2020-12-01/hgmm_5k_v3 \
--barcode_start 1 \
--barcode_length 16 \
--subset_name graft \
--lower_threshold 0.0 \
--upper_threshold 0.1 \
--threads 16 \
--compression_level 1
##----------------------------------------------------------------------------##
## XenoCell: Extract cellular barcodes.
##----------------------------------------------------------------------------##
## Input folder:               /hpcnfs/scratch/PGP/xenocell/test_runs/2020-12-01/hgmm_5k_v3
## Barcode starting position:  1
## Length of cellular barcode: 16
## Subset name:                graft
## Lower threshold:            0.0
## Upper threshold:            0.1
## Number of threads:          16
## Compression level:          1
##----------------------------------------------------------------------------##
# [2020-12-01 20:52:40] Load output data from read classification step...
# [2020-12-01 20:53:29] Generate list of cellular barcodes that pass the specified thresholds...
# [2020-12-01 20:53:34] Generate list of reads that belong to the cellular barcodes...
# [2020-12-01 21:23:57] Extract reads from original FASTQ files...
# [2020-12-01 22:12:18] Compress FASTQ files...
# [2020-12-01 22:23:19] Delete temporary files...
# [2020-12-01 22:23:21] Extraction of cellular barcodes finished!
#  - Number of unique cellular barcodes extracted: 2,998,461
#  - Number of reads extracted: 178,329,000
```

This step creates a plot highlighting the cellular barcodes which are extracted.
In this case, it looks as shown below.

<p align="center"><img src="host_vs_graft_total_subset_graft.png" /></p>

In the input folder (`/hpcnfs/scratch/PGP/xenocell/test_runs/2020-12-01_hgmm_5k_v3`) we now have an additional directory with the same name set as `subset_name`.
This folder contains the list of cellular barcodes associated with this subset, as well as the `R1` and `R2` FASTQ files which can then be used for further analysis.

### Step B: Extract host-specific cellular barcodes

Similar to before, to extract **host**-specific cellular barcodes, we use the `extract_cellular_barcodes` function to extract reads from all cellular barcodes that contain between 90-100% of host-specific reads.

__Note__:
We could omit the `--upper_threshold` parameter since the default is `1.0`.

```sh
singularity exec \
--bind /hpcnfs \
/hpcnfs/scratch/PGP/xenocell/container/xenocell_1.0.simg \
xenocell.py extract_cellular_barcodes \
--input /hpcnfs/scratch/PGP/xenocell/test_runs/2020-12-01/hgmm_5k_v3 \
--barcode_start 1 \
--barcode_length 16 \
--subset_name host \
--lower_threshold 0.9 \
--upper_threshold 1.0 \
--threads 16 \
--compression_level 1
##----------------------------------------------------------------------------##
## XenoCell: Extract cellular barcodes.
##----------------------------------------------------------------------------##
## Input folder:               /hpcnfs/scratch/PGP/xenocell/test_runs/2020-12-01/hgmm_5k_v3
## Barcode starting position:  1
## Length of cellular barcode: 16
## Subset name:                host
## Lower threshold:            0.9
## Upper threshold:            1.0
## Number of threads:          16
## Compression level:          1
##----------------------------------------------------------------------------##
# [2020-12-01 22:23:29] Load output data from read classification step...
# [2020-12-01 22:24:14] Generate list of cellular barcodes that pass the specified thresholds...
# [2020-12-01 22:24:17] Generate list of reads that belong to the cellular barcodes...
# [2020-12-01 22:54:06] Extract reads from original FASTQ files...
# [2020-12-01 23:25:46] Compress FASTQ files...
# [2020-12-01 23:31:48] Delete temporary files...
# [2020-12-01 23:31:50] Extraction of cellular barcodes finished!
#  - Number of unique cellular barcodes extracted: 1,834,132
#  - Number of reads extracted: 96,725,214
```

<p align="center"><img src="host_vs_graft_total_subset_host.png" /></p>

## Align FASTQ

We use Cell Ranger `v3.0.2` to align the FASTQ files generated by XenoCell to the `hg19`, `mm10` and combined `hg19_mm10` reference genomes (all of them version `3.0.0`) provided on the [10x Genomics website](https://support.10xgenomics.com/single-cell-gene-expression/software/downloads/latest).

For details, please refer to the respective [README file](data/).

## Down-stream analysis with Seurat

Down-stream analysis was performed separately for...

* [graft-specific cells (human; hg19)](analysis/hg19/)
* [host-specific cells (mouse; mm10)](analysis/mm10/)

We also compared the XenoCell results with the droplet classification by Cell Ranger (when using the combined reference genome `hg19_mm10`) which can be found in the [`hg19_mm10`](analysis/hg19_mm10/) folder.
