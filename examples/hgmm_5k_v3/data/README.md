# Primary analysis of `hgmm_5k_v3` example data set

* Generation of transcript count matrices from FASTQ files was done with Cell Ranger v3.0.2.
* Check individual `.sh` files (one for each sample) for details.

```bash
cellranger count \
--id=<sample_name> \
--transcriptome=<reference_genome> \
--fastqs=<path_to_FASTQ_files> \
--sample=<sample_id> \
--expect-cells=10000 \
--jobmode=local \
--localcores=12 \
--localmem=60 \
--mempercore=5 \
--chemistry=threeprime
```

For down-stream analysis, please check the respective [README file](../analysis/).

## `hg19`

### unfiltered

```bash
cellranger count \
--id=unfiltered \
--transcriptome=/hpcnfs/data/PGP/reference_genomes/CellRanger/refdata-cellranger-hg19-3.0.0 \
--fastqs=/hpcnfs/scratch/PGP/xenocell/FASTQ_files/10Xdataset/hgmm_5k_v3_fastqs \
--sample=hgmm_5k_v3 \
--expect-cells=10000 \
--jobmode=local \
--localcores=12 \
--localmem=60 \
--mempercore=5
```

### graft

```bash
cellranger count \
--id=graft \
--transcriptome=/hpcnfs/data/PGP/reference_genomes/CellRanger/refdata-cellranger-hg19-3.0.0 \
--fastqs=/hpcnfs/scratch/PGP/xenocell/example/2019-04-18_Sample_hgmm_5k_v3/00_FASTQ/graft \
--sample=hgmm_5k_v3 \
--expect-cells=10000 \
--jobmode=local \
--localcores=12 \
--localmem=60 \
--mempercore=5
```

## `mm10`

### unfiltered

```bash
cellranger count \
--id=unfiltered \
--transcriptome=/hpcnfs/data/PGP/reference_genomes/CellRanger/refdata-cellranger-mm10-3.0.0 \
--fastqs=/hpcnfs/scratch/PGP/xenocell/FASTQ_files/10Xdataset/hgmm_5k_v3_fastqs \
--sample=hgmm_5k_v3 \
--expect-cells=10000 \
--jobmode=local \
--localcores=12 \
--localmem=60 \
--mempercore=5
```

### host

```bash
cellranger count \
--id=host \
--transcriptome=/hpcnfs/data/PGP/reference_genomes/CellRanger/refdata-cellranger-mm10-3.0.0 \
--fastqs=/hpcnfs/scratch/PGP/xenocell/example/2019-04-18_Sample_hgmm_5k_v3/00_FASTQ/host \
--sample=hgmm_5k_v3 \
--expect-cells=10000 \
--jobmode=local \
--localcores=12 \
--localmem=60 \
--mempercore=5 \
--chemistry=threeprime
```

## `hg19+mm10`

### unfiltered

```bash
cellranger count \
--id=unfiltered \
--transcriptome=/hpcnfs/data/PGP/reference_genomes/CellRanger/refdata-cellranger-hg19-and-mm10-3.0.0 \
--fastqs=/hpcnfs/scratch/PGP/xenocell/FASTQ_files/10Xdataset/hgmm_5k_v3_fastqs \
--sample=hgmm_5k_v3 \
--expect-cells=10000 \
--jobmode=local \
--localcores=12 \
--localmem=60 \
--mempercore=5
```










